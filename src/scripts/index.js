import '../styles/index.scss';
import * as moment from 'moment';
import 'bootstrap/dist/css/bootstrap.min.css';

$(document).ready(()=>{
    
    console.log('Hello jQuery');

    var allPanels = $('.accordian > .card > .card-body > p').hide();
    const allText = $('.accordian > .card > .card-header > h5');

    $('.accordian > .card > .card-header > h5').click(function() {
        allPanels.slideUp();
        let textString = $(this).parent().next().children();

        //if(textString.val() === "false") {
           // textString.val("true");
            textString.slideDown("slow");
            return false;
        //}  else {
            //allText.val("false");
            //return false;
       // }
      });

    /**
     * Documentation for the slider
     * https://bxslider.com/examples/auto-show-start-stop-controls/ 
     */ 

    $(function(){
        $('.bxslider').bxSlider({
          mode: 'fade',
          captions: true,
          slideWidth: 600
        });
      });
     /**
     * Documentation for the Lightbox - Fancybox
     * See the section 5. Fire plugin using jQuery selector.
     * http://fancybox.net/howto
     */ 
        

        $("a.group").fancybox({
            'transitionIn'	:	'elastic',
            'transitionOut'	:	'elastic',
            'speedIn'		:	600, 
            'speedOut'		:	200, 
            'overlayShow'	:	false,
            //'onComplete' : function(){$('.closer').click(function(){parent.$.fancybox.close();})}
        });

     /**
      * Boostrap Modal (For the Learn More button)
      * https://getbootstrap.com/docs/4.0/components/modal/
      */
    

});

/**
 * REMEMBER!!
 * Declaring Global functions that are accessible in your HTML.
 */ 
window.helloWorld = function() {
    console.log('HEllooOooOOo!');
};
